package Lab2;

public class car { //odometer, brand, color,and gear
	
	private int odometer;
	private String brand;
	private String color;
	private int gear;
	
	
	public car(int odometer, String brand, String color, int gear) {
		this.odometer = odometer;
		this.brand = brand;
		this.color = color;
		this.gear = gear;
	}
	
//Implement methods incrementGear() and decrementGear() to increment or decrement the 	gear. It is initially set to 0.
	
	public void incrementGear() {
		this.gear = this.gear + 1 ;
		System.out.println("Gear is changed suscessfuly");
		System.out.println("Gear:" + this.gear);
	}
	public void decrementGear() {
		this.gear = this.gear - 1 ;
		System.out.println("Gear is changed suscessfuly");
		System.out.println("Gear:" + this.gear);
	}

//Implement a drive method which takes two arguments: (1) Number of hours traveled and (2)
//	km traveled per hour. This function is going to be used to modify the odometer. 
	
	public void drivemethod(int hours, int kmph) {
		
	}
	
	public void display(){
		System.out.println("Odometer:" + this.odometer);
		System.out.println("Color:" + this.color);
		System.out.println("Brand:" + this.brand);
		System.out.println("Gear:" + this.gear);
	}
	
	
	
	public int getOdometer() {
		return odometer;
	}


	public void setOdometer(int odometer) {
		this.odometer = odometer;
	}


	public String getBrand() {
		return brand;
	}


	public void setBrand(String brand) {
		this.brand = brand;
	}


	public String getColor() {
		return color;
	}


	public void setColor(String color) {
		this.color = color;
	}


	public int getGear() {
		return gear;
	}


	public void setGear(int gear) {
		this.gear = gear;
	}
	
	
}
